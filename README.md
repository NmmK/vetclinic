# VetClinic
# Консольное приложение, имитирующее работу ветеринарной клиники.

*Исхаков Тимур*

## Описание

Проект создан в рамках практики в Академии разработки MediaSoft.
Взаимодействие с приложением осуществляется через команды.

Примеры команд:

```
create patient Мурзик 3 Кот
create patient Барсик 2 Собака

change patient's name 2 Снежок
change patient's name 1 Муся

create doctor Иван Иванович Петров 45 хирург
create doctor Петр Олегович Иванов 52 кардиолог

create appointment 12.12.2022 11:00

patients

delete patient 2

patient appointments 2

change appointment status
