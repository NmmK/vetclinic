import command.CommandReader;

public class Main {
    public static void main(String[] args) {

        authenticate();

    }

    private static void authenticate() {
        Authentication authentication = new Authentication();
        int resultCode = authentication.authenticate();

        if (resultCode == 0){
            System.out.println("Login is succeeded");
            CommandReader.startReading();

        } else {
            System.out.println("Login is failed");
        }
    }
}
