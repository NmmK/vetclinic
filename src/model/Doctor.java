package model;

public class Doctor {
    private static int count = 0;

    private int id;

    private int age;

    private String name;

    private String surname;

    private String patronym;

    private String specialty;

    public Doctor(int age, String name, String surname, String patronym, String specialty) {
        this.id = ++count;
        this.age = age;
        this.name = name;
        this.surname = surname;
        this.patronym = patronym;
        this.specialty = specialty;
    }

    public Doctor() {

    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronym() {
        return patronym;
    }

    public String getSpecialty() {
        return specialty;
    }
}
