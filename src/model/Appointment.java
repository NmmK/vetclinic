package model;

public class Appointment {

    private static int count = 0;

    private int id;

    private String date;

    private String time;

    private String status;

    private Doctor doctor;

    private AnimalPatient patient;

    public Appointment(String date, String time, String status, Doctor doctor, AnimalPatient patient) {
        this.date = date;
        this.time = time;
        this.status = status;
        this.doctor = doctor;
        this.patient = patient;
        this.id = ++count;
    }

    public Appointment() {

    }

    public int getId() {
        return id;
    }
    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getStatus() {
        return status;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public AnimalPatient getPatient() {
        return patient;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Id: " + id + ". Date: " + date + ". Time: " + time + ". Status: " + status +
                ".\n Patient's Id: " + patient.getId() + ". PatientName: " + patient.getName() +
                ". Patient's species: " + patient.getSpecies() + ".\n Doctor's Id: " + doctor.getId() +
                ". Fullname: " + doctor.getName() + " " + doctor.getSurname() + " " + doctor.getPatronym() +
                ". Specialty: " + doctor.getSpecialty() + " \n";
    }


}
