package model;

public enum StatusType
{
    NEW,
    IN_PROGRESS,
    CANCELLED,
    AWAITING_PAYMENT,
    COMPLETED;
}