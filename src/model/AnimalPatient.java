package model;

import java.time.LocalDate;
import java.util.Date;

public class AnimalPatient {

    private static int count = 0;

    private int id;

    private int age;

    private String name;

    private String species;

    private LocalDate dateOfRegistration;

    public AnimalPatient(String name, int age, String species) {
        this.name = name;
        this.age = age;
        this.species = species;
        this.dateOfRegistration = LocalDate.now();
        this.id = ++count;
    }

    public AnimalPatient() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDateOfRegistration() {
        return dateOfRegistration;
    }

    public String getSpecies() {
        return species;
    }
}
