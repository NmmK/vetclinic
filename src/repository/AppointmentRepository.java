package repository;

import model.Appointment;

import java.util.Set;

public interface AppointmentRepository {
    Set<Appointment> findAll();

    void save(Appointment appointment);

    void remove(Appointment appointment);

    void changeStatus(Appointment appointment, String status);

    void displayAppointments();
}
