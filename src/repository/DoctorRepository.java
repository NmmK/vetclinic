package repository;

import model.Doctor;

import java.util.Set;

public interface DoctorRepository {
    Set<Doctor> findAll();

    void save(Doctor doctor);

    void displayDoctors();
}
