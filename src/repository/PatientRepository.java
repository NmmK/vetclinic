package repository;

import model.AnimalPatient;

import java.util.Set;

public interface PatientRepository {
    Set<AnimalPatient> findAll();

    void save(AnimalPatient patient);

    void remove(AnimalPatient patient);

    void changeName(AnimalPatient patient, String name);

    void displayPatients();
}
