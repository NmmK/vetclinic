package repository.impl;

import model.AnimalPatient;
import model.Doctor;
import repository.DoctorRepository;

import java.util.HashSet;
import java.util.Set;

public class DoctorRepositoryImpl implements DoctorRepository {

    private static final Set<Doctor> DOCTORS = new HashSet<>();
    private static final DoctorRepositoryImpl SINGLETON = new DoctorRepositoryImpl();

    public DoctorRepositoryImpl() {
    }

    public static DoctorRepositoryImpl getSingleton() {
        return SINGLETON;
    }

    @Override
    public Set<Doctor> findAll() {
        return DOCTORS;
    }

    @Override
    public void save(Doctor doctor) {
        DOCTORS.add(doctor);
    }

    @Override
    public void displayDoctors() {
        for (Doctor doctor : DOCTORS) {

            System.out.printf("Id: \"%s\". Fullname: %s %s %s. Specialty: %s Age: %d \n",
                    doctor.getId(),
                    doctor.getName(),
                    doctor.getSurname(),
                    doctor.getPatronym(),
                    doctor.getSpecialty(),
                    doctor.getAge()
                    );
        }
    }
}
