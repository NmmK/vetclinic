package repository.impl;

import model.AnimalPatient;
import repository.PatientRepository;

import java.util.HashSet;
import java.util.Set;

public class PatientRepositoryImpl implements PatientRepository {

    private static final Set<AnimalPatient> PATIENTS = new HashSet<>();

    private static final PatientRepositoryImpl SINGLETON = new PatientRepositoryImpl();

    public PatientRepositoryImpl() {
    }

    public static PatientRepositoryImpl getSingleton() {
        return SINGLETON;
    }

    @Override
    public Set<AnimalPatient> findAll() {
        return PATIENTS;
    }

    @Override
    public void save(AnimalPatient patient) {
        PATIENTS.add(patient);
    }

    @Override
    public void remove(AnimalPatient patient) {
        PATIENTS.remove(patient);
    }

    @Override
    public void changeName(AnimalPatient patient, String name) {
        patient.setName(name);
    }

    @Override
    public void displayPatients() {
        for (AnimalPatient patient : PATIENTS) {
            var dateOfRegistration = patient.getDateOfRegistration().toString().substring(0,10);

            System.out.printf("Id: \"%s\". Name: \"%s\". Species: \"%s\". Date of Registration: %s \n",
                    patient.getId(),
                    patient.getName(),
                    patient.getSpecies(),
                    dateOfRegistration
            );
        }

    }
}
