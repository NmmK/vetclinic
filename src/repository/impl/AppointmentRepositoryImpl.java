package repository.impl;

import model.AnimalPatient;
import model.Appointment;
import repository.AppointmentRepository;

import java.util.HashSet;
import java.util.Set;

public class AppointmentRepositoryImpl implements AppointmentRepository {

    private static final Set<Appointment> APPOINTMENTS = new HashSet<>();

    private static final AppointmentRepositoryImpl SINGLETON = new AppointmentRepositoryImpl();

    private AppointmentRepositoryImpl() {
    }

    public static AppointmentRepositoryImpl getSingleton() {
        return SINGLETON;
    }

    @Override
    public Set<Appointment> findAll() {
        return APPOINTMENTS;
    }

    @Override
    public void save(Appointment appointment) {
        APPOINTMENTS.add(appointment);
    }

    @Override
    public void remove(Appointment appointment) {
        APPOINTMENTS.remove(appointment);
    }

    @Override
    public void changeStatus(Appointment appointment, String status) {
       appointment.setStatus(status);
    }

    @Override
    public void displayAppointments() {
        for (Appointment appointment : APPOINTMENTS) {
            var doctor = appointment.getDoctor();
            var patient = appointment.getPatient();

            System.out.printf("Id: %d Date %s Time %s Status %s Doctor %s %s %s Patient %s",
                appointment.getId(), appointment.getDate(), appointment.getTime(),
                    appointment.getStatus(), doctor.getName(), doctor.getSurname(), doctor.getPatronym(),
                    patient.getName()
            );
        }

    }

}
