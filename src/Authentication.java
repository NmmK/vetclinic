import java.util.Scanner;

public class Authentication {
    private static final String LOGIN = "login";
    private static final String PASSWORD = "1234";

    public int authenticate() {
        Scanner s = new Scanner(System.in);

        int countOfLoginAttempts = 3;
        boolean isAuthenticationSuccess = false;

        while (!isAuthenticationSuccess  && countOfLoginAttempts > 0){

            System.out.println("Login: ");
            var login = s.nextLine();

            System.out.println("Password: ");
            var password = s.nextLine();

            if (validate(login, password)){
                isAuthenticationSuccess = true;
            } else {
                System.out.println("Password is incorrect. Please try again. ");
            }
            countOfLoginAttempts--;
        }

      return isAuthenticationSuccess? 0 : 1;

    }




    public boolean validate(String login, String password) {
        return login.equals(LOGIN) && password.equals(PASSWORD);
    }
}
