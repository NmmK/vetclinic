package command;

import model.Appointment;

import java.util.Map;
import java.util.Scanner;
import command.executor.*;

public class CommandReader {

    private static final Map<CommandType, CommandExecutor> COMMAND_EXECUTORS_GROUPED_BY_COMMAND = Map.of(
            CommandType.CREATE_DOCTOR, new DoctorCreator(),
            CommandType.CREATE_PATIENT, new PatientCreator(),
            CommandType.CREATE_APPOINTMENT, new AppointmentCreator(),
            CommandType.DELETE_PATIENT, new PatientDeleter(),
            CommandType.DISPLAY_ALL_PATIENTS, new PatientsDisplayer(),
            CommandType.DISPLAY_ALL_PATIENT_APPOINTMENTS, new PatientAppointmentsDisplayer(),
            CommandType.CHANGE_PATIENTS_NAME, new PatientsNameChanger(),
            CommandType.CHANGE_APPOINTMENT_STATUS, new AppointmentStatusChanger()
    );

    private static CommandType getCommandType(String command) {
        if (command.contains("create doctor")) {
            return CommandType.CREATE_DOCTOR;
        }

        if (command.contains("create patient")) {
            return CommandType.CREATE_PATIENT;
        }

        if (command.contains("create appointment")) {
            return CommandType.CREATE_APPOINTMENT;
        }

        if (command.contains("delete patient")) {
            return CommandType.DELETE_PATIENT;
        }

        if (command.contains("patients")) {
            return CommandType.DISPLAY_ALL_PATIENTS;
        }

        if (command.contains("patient appointments")) {
            return CommandType.DISPLAY_ALL_PATIENT_APPOINTMENTS;
        }

        if (command.contains("change patient's name")) {
            return CommandType.CHANGE_PATIENTS_NAME;
        }

        if (command.contains("change appointment status")) {
            return CommandType.CHANGE_APPOINTMENT_STATUS;
        }

        if (command.contains("exit")) {
            return CommandType.EXIT;
        }

        return CommandType.UNDEFINED;
    }

    private static int readCommand(Scanner s) {
        var command = s.nextLine();

        CommandType commandType = getCommandType(command);

        if (COMMAND_EXECUTORS_GROUPED_BY_COMMAND.containsKey(commandType)) {
            var commandExecutor = COMMAND_EXECUTORS_GROUPED_BY_COMMAND.get(commandType);
            return commandExecutor.execute(command);
        }

        if (commandType == CommandType.EXIT) {
            return 0;
        }

        System.out.println("Incorrect command");
        return -1;
    }

    public static void startReading(){
        System.out.println("Write your command:");
        Scanner s = new Scanner(System.in);
        int code = 1;
        while (code != 0) {
            code = readCommand(s);
        }

        s.close();

    }

}
