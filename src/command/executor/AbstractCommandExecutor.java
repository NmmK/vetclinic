package command.executor;

import repository.AppointmentRepository;
import repository.DoctorRepository;
import repository.PatientRepository;
import repository.impl.AppointmentRepositoryImpl;
import repository.impl.DoctorRepositoryImpl;
import repository.impl.PatientRepositoryImpl;
import model.*;

import java.util.Optional;

public abstract class AbstractCommandExecutor implements CommandExecutor {

    protected final DoctorRepository doctorRepository = DoctorRepositoryImpl.getSingleton();

    protected final PatientRepository patientRepository = PatientRepositoryImpl.getSingleton();

    protected final AppointmentRepository appointmentRepository = AppointmentRepositoryImpl.getSingleton();

    protected AnimalPatient findPatient(int id) {
        for (AnimalPatient patient : patientRepository.findAll()) {
            if (patient.getId() == id) {
                return patient;
            }
        }
        return new AnimalPatient();
    }

    protected Doctor findDoctor(int id) {
        for (Doctor doctor : doctorRepository.findAll()) {
            if (doctor.getId() == id) {
                return doctor;
            }
        }
        return new Doctor();
    }

    protected Appointment findAppointment(int id) {
        for (Appointment appointment : appointmentRepository.findAll()) {
            if (appointment.getId() == id) {
                return appointment;
            }
        }
        return new Appointment();
    }

}
