package command.executor;

import command.CommandType;
import model.Appointment;

import java.util.Scanner;
import java.util.Set;

public class AppointmentStatusChanger extends AbstractCommandExecutor{
    public int execute(String command) {
        return changeStatus(command);
    }

    @Override
    public CommandType getCommandType() {
        return CommandType.CHANGE_APPOINTMENT_STATUS;
    }

    private int changeStatus(String command) {

        var wordsArray = command.split(" ");
        var appointmentlId = Integer.parseInt(wordsArray[3]);

        Appointment temp = findAppointment(appointmentlId);

        if (temp.getId() == 0) {
            System.out.println("Appointment doesn't exist");
        } else {
            System.out.println("Choose status: ");

            AppointmentCreator status = new AppointmentCreator();
            var newStatus = status.chooseStatus();

            appointmentRepository.changeStatus(temp, newStatus);
            System.out.println("Appointment's status is changed");
        }

        return 1;
    }
}