package command.executor;

import command.CommandType;
import model.Doctor;

import java.util.Scanner;

public class DoctorCreator extends AbstractCommandExecutor{
    @Override
    public int execute(String command) {
        return createDoctor(command);
    }

    @Override
    public CommandType getCommandType() {
        return CommandType.CREATE_DOCTOR;
    }

    private int createDoctor(String command) {

        var wordsArray = command.split(" ");

        var doctorName = wordsArray[2];
        var doctorSurname = wordsArray[3];
        var doctorPatronym = wordsArray[4];
        var doctorAge = Integer.parseInt(wordsArray[5]);
        var doctorSpecialty = wordsArray[6];

        Doctor newDoctor = new Doctor(doctorAge, doctorName, doctorSurname,
                doctorPatronym, doctorSpecialty);

        doctorRepository.save(newDoctor);

        System.out.println("Doctor is created");

        return 1;
    }
}
