package command.executor;

import command.CommandType;
import model.AnimalPatient;

import java.util.Optional;
import java.util.Scanner;

public class PatientsNameChanger extends AbstractCommandExecutor{
    @Override
    public int execute(String command) {
        return changeName(command);
    }

    @Override
    public CommandType getCommandType() {
        return CommandType.CHANGE_PATIENTS_NAME;
    }

    private int changeName(String command) {

        var wordsArray = command.split(" ");

        var animalId = Integer.parseInt(wordsArray[3]);
        var animalName = wordsArray[4];

        AnimalPatient temp = findPatient(animalId);

        if (temp.getName() == null) {
            System.out.println("Patient doesn't exist");
        } else {
            patientRepository.changeName(temp, animalName);
            System.out.println("Patient's name is changed");
        }

        return 1;
    }
}
