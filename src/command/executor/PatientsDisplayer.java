package command.executor;

import command.CommandType;
import model.AnimalPatient;

public class PatientsDisplayer extends AbstractCommandExecutor{
    @Override
    public int execute(String command) {
        return displayPatients(command);
    }

    @Override
    public CommandType getCommandType() {
        return CommandType.DISPLAY_ALL_PATIENTS;
    }

    private int displayPatients(String command) {
        patientRepository.displayPatients();
        return 1;
    }

}
