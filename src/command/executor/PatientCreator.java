package command.executor;

import command.CommandType;
import model.AnimalPatient;
import model.Doctor;
import repository.PatientRepository;

import java.util.Scanner;

public class PatientCreator extends AbstractCommandExecutor{
    @Override
    public int execute(String command) {
        return createPatient(command);
    }

    @Override
    public CommandType getCommandType() {
        return CommandType.CREATE_PATIENT;
    }

    private int createPatient(String command) {

        var wordsArray = command.split(" ");

        var animalName = wordsArray[2];
        var animalAge = Integer.parseInt(wordsArray[3]);
        var species =  wordsArray[4];

        AnimalPatient newPatient = new AnimalPatient(animalName, animalAge, species);

        patientRepository.save(newPatient);

        System.out.println("Patient is created");

        return 1;
    }
}