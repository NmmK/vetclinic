package command.executor;

import command.CommandType;
import model.Appointment;

import java.util.Scanner;
import java.util.Set;

public class PatientAppointmentsDisplayer extends AbstractCommandExecutor{
    public int execute(String command) {
        return displayPatientsAppointments(command);
    }

    @Override
    public CommandType getCommandType() {
        return CommandType.DISPLAY_ALL_PATIENT_APPOINTMENTS;
    }

    private int displayPatientsAppointments(String command) {

        var wordsArray = command.split(" ");

        var patientId = Integer.parseInt(wordsArray[2]);

        Set<Appointment> appointments = appointmentRepository.findAll();
        int patientAppointmentsCount = 0;

        for (Appointment appointment: appointments) {
            if (appointment.getPatient().getId() == patientId){
                System.out.print(appointment);
                patientAppointmentsCount++;
            }
        }

        if (patientAppointmentsCount == 0){
            System.out.println("Patient doesn't have appointment or doesn't exist ");
        }

        return 1;
    }
}