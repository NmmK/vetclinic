package command.executor;

import command.CommandType;
import model.AnimalPatient;
import model.Appointment;
import model.Doctor;
import model.StatusType;

import java.util.Scanner;

public class AppointmentCreator extends AbstractCommandExecutor{
    @Override
    public int execute(String command) {
        return createAppointemnt(command);
    }

    @Override
    public CommandType getCommandType() {
        return CommandType.CREATE_APPOINTMENT;
    }

    private int createAppointemnt(String command) {

        var wordsArray = command.split(" ");

        var date = wordsArray[2];
        var time = wordsArray[3];

        var status = chooseStatus();

        System.out.println("Choose doctor: ");
        var doctor = chooseDoctor();

        if (doctor.getName() == null) {
            System.out.println("Doctor doesn't exist");
            return 1;
        }

        System.out.println("Choose patient: ");
        var patient = choosePatient();

        if (patient.getName() == null) {
            System.out.println("Patient doesn't exist");
        } else {

            Appointment newAppointment = new Appointment(date, time, status, doctor, patient);

            appointmentRepository.save(newAppointment);

            System.out.println("Appointment is created");
        }
        return 1;

    }

    private Doctor chooseDoctor() {
        doctorRepository.displayDoctors();

        System.out.println("Choose doctor(enter the id): ");

        Scanner s = new Scanner(System.in);
        var idOfDoctor = s.nextInt();

        return findDoctor(idOfDoctor);
    }

    private AnimalPatient choosePatient() {
        patientRepository.displayPatients();

        System.out.println("Choose patient(enter the id): ");

        Scanner s = new Scanner(System.in);
        var idOfPatient = s.nextInt();

        return findPatient(idOfPatient);
    }

    public String chooseStatus(){

        StatusType[] types = StatusType.values();

        int i = 1;
        for (StatusType status : types) {
            System.out.printf("%d - ", i);
            System.out.println(status);
            i++;
        }

        System.out.println("Choose status(enter the index): ");

        Scanner s = new Scanner(System.in);
        var indexOfStatus = s.nextInt();

        switch (indexOfStatus) {
            case 1:
                return "новый";
            case 2:
                return "в процессе";
            case 3:
                return "отменен";
            case 4:
                return "ожидает оплаты";
            case 5:
                return "завершен";
            default:
                break;
        }

        return "";
    }
}



