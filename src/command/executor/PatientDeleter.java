package command.executor;

import command.CommandType;
import model.AnimalPatient;
import model.Appointment;

import java.util.Optional;
import java.util.Scanner;
import java.util.Set;

public class PatientDeleter extends AbstractCommandExecutor {
    @Override
    public int execute(String command) {
        return deletePatient(command);
    }

    @Override
    public CommandType getCommandType() {
        return CommandType.DELETE_PATIENT;
    }

    private int deletePatient(String command) {

        var wordsArray = command.split(" ");

        var animalId = Integer.parseInt(wordsArray[2]);

        AnimalPatient temp = findPatient(animalId);

        if (temp.getName() == null) {
            System.out.println("Patient doesn't exist");
        } else {
            Set<Appointment> appointments = appointmentRepository.findAll();
            for (Appointment appointment: appointments) {
                if (appointment.getPatient().getId() == temp.getId()){
                    appointmentRepository.remove(appointment);
                }
            }
            patientRepository.remove(temp);
            System.out.println("Patient is deleted");
        }

        return 1;
    }

}